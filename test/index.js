var klvPlayer1;


$(document).ready(function () {

    // Init configuration variables
    setLocalStorage();

    // Create player and set some basic configuration params
    klvPlayer1 = new KlvPlayer("#klvPlayer1");
    klvPlayer1.user = localStorage.user;
    klvPlayer1.password = localStorage.password;
    klvPlayer1.setServerUrl(localStorage.serverUrl);

    // Bind control events
    setGuiEvents(klvPlayer1);

    klvPlayer1.showPoster('./img/StanagOnDemand.jpg');

    // Create a table for KLV presentation
    createTelemetryTable();

    // Get mission list, fill GUI and start a first available mission playback
    klvPlayer1.getMissions(function (err, missions) {

        if (!err) {
            $.each(missions, function (index, mission) {
                $("#missionSelect").append("<option value='" + mission._id + "'>" + mission.name + "</option>");
            });

            if (missions.length) {
                clearKlvTableValues();
                klvPlayer1.setMission(missions[0]._id);
                klvPlayer1.start();
            }
        }
        else {
            reportEvent("#player1Messages", err.statusText);
            alert("klvPlayer1 error: " + err.statusText);
        }
    });




    // Player events

    klvPlayer1.on('Connected', function () {
        reportEvent("#player1Messages", 'Connected to server');
    });

    klvPlayer1.on('Disconnected', function () {
        reportEvent("#player1Messages", 'Disconnected from server');
    });

    klvPlayer1.on('Started', function () {
        reportEvent("#player1Messages", 'Started');
    });

    klvPlayer1.on('Paused', function () {
        reportEvent("#player1Messages", 'Paused');
    });

    klvPlayer1.on('Stopped', function () {
        reportEvent("#player1Messages", 'Stopped');
    });

    klvPlayer1.on('SeekStarted', function () {
        reportEvent("#player1Messages", 'SeekStarted');
    });

    klvPlayer1.on('SeekComplete', function () {
        reportEvent("#player1Messages", 'SeekComplete');
    });

    klvPlayer1.on('Running', function () {
        reportEvent("#player1Messages", 'Running');
    });

  
    klvPlayer1.on("Resize", function () {
        var message = "Size Changed " + this.getWidth() + "x" + this.getHeight();
        reportEvent("#player1Messages", message);
        $("#resizable").height($("#resizable").height() + parseInt($("#resizable").css('padding-top').replace("px", "")));
    });

    klvPlayer1.on("VideoSizeChanged", function () {

        var w = this.getNaturalWidth();
        var h = this.getNaturalHeight();
        var message = "Resolution Changed " + w + "x" + h;
        reportEvent("#player1Messages", message);
        var aspRatio = w / h;
        $("#resizable").height($("#resizable").innerWidth() / aspRatio );
        $("#resizable").resizable("option", "aspectRatio", aspRatio);
    });
    
    klvPlayer1.on("FullScreenOn", function () {
        reportEvent("#player1Messages", "FullScreen On");
    });
    
    klvPlayer1.on("FullScreenOff", function () {
        reportEvent("#player1Messages", "FullScreen Off");
    });
    
    klvPlayer1.on("SignalLost", function () {
        reportEvent("#player1Messages", "Signal Lost");
    });

    klvPlayer1.on("DataArrived", function (data) {
        // reportEvent("#player1Messages", "Data arrived " + data.timestamp);     
        setTimeout(function () { updateTelemetry(data); }, 1);
    });

    klvPlayer1.on("Ended", function () {
        reportEvent("#player1Messages", "Playback complete");
    });

    klvPlayer1.on("Message", function (message) {
        reportEvent("#player1Messages", message);
    });

    klvPlayer1.on("Error", function (message) {
        reportEvent("#player1Messages", message);
        alert("klvPlayer1 error: " + message);
    });
});


/**
 * initiate Local Storage
 */
function setLocalStorage() {

    if (!localStorage.user)
        localStorage.user = "guest";
    $("#user").val(localStorage.user);

    if (!localStorage.password)
        localStorage.password = "guest";
    $("#password").val(localStorage.password);

    if (!localStorage.serverUrl)
        localStorage.serverUrl = 'http://vod-aws.impleotv.com:80/';
    $("#serverUrl").val(localStorage.serverUrl);
}

/**
 * setup GuiEvents
 * @param  {any} player
 */
function setGuiEvents(player) {

    $("#resizable").resizable({
        aspectRatio: true
    });

    $("#serverUrl").on('change keyup paste', function () {
        localStorage.serverUrl = $("#serverUrl").val();
        player.serverUrl = localStorage.serverUrl;
    });

    $('#serverUrl').keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            alert('Please reload the page.');
        }
    });

    $("#user").on('change keyup paste', function () {
        localStorage.user = $("#user").val();
        player.user = localStorage.user;
    });

    $("#password").on('change keyup paste', function () {
        localStorage.password = $("#password").val();
        player.password = localStorage.password;
    });

    $('#missionSelect').change(function () {
        var optionSelected = $(this).find("option:selected");
        clearKlvTableValues();
        player.setMission(optionSelected.val());
        player.start();
    });

    $("#player1Effect").on('change', function (e) {
        var optionSelected = $("option:selected", this);
        player.effect(this.value);
    });

    $("#clearMessagesBttn").click(function () {
        $("#player1Messages").empty();
    });
}

/**
 * @param  {any} player
 * @param  {any} message
 */
function reportEvent(player, message) {

    $(player).width($('#resizable').width());

    var $newOption = $('<option>' + moment().format('MMMM Do YYYY, h:mm:ss.SSS a - ') + message + '</option>');
    $(player).append($newOption);
    $(player).scrollTop($newOption.position().top);
}

/**
 * create Telemetry Table
 */
function createTelemetryTable() {

    var KlvTable = $('#klvTable').DataTable({
        "aLengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        stateSave: true,
        deferRender: true,
        columns: [
            {
                "width": "5%",
                "sClass": "center"
            },
            {},
            {}
        ]
    });

    for (var tag = 1; tag < 94; tag++) {
        KlvTable.row.add([tag, klvPlayer1.klvHelper.tag2Name(tag), '-']);
    }
    KlvTable.draw();
}

/**
 * @param  {any} klvPckt
 */
function updateTelemetry(klvPckt) {
    var table = $('#klvTable').DataTable();

    // we don't clear old values as it takes too much time. You should probably write something more sofisticated, if it is important.   
    if (klvPckt) {
        for (var tag in klvPckt.klvs) {
            var conv = klvPlayer1.klvHelper.tagConv(tag, klvPckt.klvs[tag]);
            var rowId = Number(tag) - 1;
            var data = table.row(rowId).data();
            if (data[2] != conv.value) {
                data[2] = conv.value;
                table.row(rowId).data(data);
            }
        }
    }
}

/**
 * clear KlvTableValues
 */
function clearKlvTableValues() {
    var table = $('#klvTable').DataTable();
    for (var i = 0; i < 93; i++) {
        var data = table.row(i).data();
        data[2] = ' ';
        table.row(i).data(data);
    }
}