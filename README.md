# KlvPlayer

**KlvPlayer** is a front-end Javascript library that delivers On-Demand / Live FMV and geo-spatial **STANAG** metadata to the client (browser).
**KlvPlayer** works with [StanagOnDemandServer](http://impleotv.com/products/stanagondemand-server/)

 ![Klv Player sample](http://www.impleotv.com/content/stanagondemand/help/KlvPlayerSampleSm.jpg)

## Main features

- On Demand Video Playback
- Low latency live video playback (LAN)
- Authentication and Authorization
- Video processing (effects)
- Parsed MISB 0601.X metadata events
- Sample code
- No browser plugins required

## Installing KlvPlayer

The primary way to obtain the software is to use the [KlvPlayer downloader](/dist). 

### Bower

If you prefer to use a package manager such as Bower, distribution repository is available under the name `klvplayer`. 


So, there are 3 installation options:

- Download the latest release: [KlvPlayer downloader](/dist). 
- Clone the repo: `https://gitlab.com/impleotv/klvplayer.git`.
- Install with [Bower](http://bower.io): `bower install klvplayer`.

### What's included

```
klvplayer/
├── dist/
│   ├── klvplayer.min.js   
├── test/
│   ├── css/
│       ├── playerdemo.css
│   ├── img/
│       ├── StanagOnDemand.jpg
│   ├── bower.json
│   ├── index.html
│   └── index.js
```

We provide compiled JS (with CSS included).


## [Online Demo ](http://vod-aws.impleotv.com/samples/klvplayer)

---

## Getting started

Include the dependencies and add klvplayer to your html page:

``` html
    ...
    <script src="bower_components/klvplayer/dist/klvplayer.min.js"></script>
    ...
    <div id="klvPlayer1" class='KlvPlayer'> </div>
    ...
```

Initialize KlvPlayer:

```javascript
    var klvPlayer1 = new KlvPlayer("#klvPlayer1");
    klvPlayer1.user =  <user>;
    klvPlayer1.password = <password>;
    klvPlayer1.setServerUrl(<server url>);
```

Setup events, if needed (more info in the sample code).

```javascript
    klvPlayer1.on('Connected', function () {

    });
    
    klvPlayer1.on('Disconnected', function () {
       
    });

    klvPlayer1.on('Started', function () {
       
    });    
    ...   
```

Get mission list and start playback

```javascript
   // Get mission list, and start the first available mission playback
   klvPlayer1.getMissions(function (err, missions) {
        if (missions.length) {         
            klvPlayer1.setMission(missions[0]._id);
            klvPlayer1.start();
        }      
    });
```

Additional info, including complete player **API** can be found [here](http://www.impleotv.com/content/stanagondemand/help/playerapi/index.html)

## Author
@[ImpleoTV Systems Ltd](https://www.impleotv.com)

