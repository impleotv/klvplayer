Change Log
==========

### Ver. 1.1.5
- Close KlvPlayer added

### Ver. 1.1.4
- Some libraries (including VideoJS) updated

### Ver. 1.1.3
- VideoJS updated

### Ver. 1.1.2
- VideoJS ahd HLS updated

### Ver. 1.1.1
- DVR mode added
- Frame by Frame added

### Ver. 1.1.0
- Live HLS updated to support KLV 


### Ver. 1.0.8
- Adaptations for new 64 capture player 
- Live renderer rewritten
- Klv Data for live mode changed from pull for push
- Color bar on signal loss fixed

### Ver. 1.0.7
- CDN Playback added

### Ver. 1.0.6
- Fixed "Add Bookmark doesn't show captured video frame"

### Ver. 1.0.5
*Server version 1.0.25*

- jsdoc annotations added.
- gulp file modified to allow player help build.


### Ver. 1.0.4
*Server version 1.0.24*

- Video is rendered directly, without copying (for on demand playback)
- KlvPlayer sample rewritten. Now uses Bower.
